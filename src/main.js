import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'


import Bot from '@/components/Bot'
import 'bootstrap/dist/css/bootstrap.css'
import './assets/bootstrap.min.css'

Vue.config.productionTip = false

Vue.use(BootstrapVue)

Vue.component('Bot', Bot)

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')