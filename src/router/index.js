import Vue from 'vue'
import Router from 'vue-router'
import Bot from '@/components/Bot'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/bot/0'
    },
    {
      path: '/bot/:botid',
      component: Bot,
      props: true
    }
  ]
})
